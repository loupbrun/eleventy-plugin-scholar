# Eleventy Scholar

CURRENTLY IN BETA.
Eleventy plugin for the blogging scholar.
Include references and create complete bibliographies thanks to [`citeproc-js`](https://github.com/Juris-M/citeproc-js).

## Usage

1. Download this package:
   ```bash
   git clone git@gitlab.com:loupbrun/eleventy-plugin-scholar.git plugins/eleventy-plugin-scholar
   ```
2. Reference it in your `eleventy.config.js` file:\
   ```js
   // Make sure to point to the correct location
   const eleventyScholar = require("./plugins/eleventy-plugin-scholar")
   
   module.exports = eleventyConfig => {
     // ...
	   eleventyConfig.addPlugin(eleventyScholar)
	   // ...
   }
   ```
3. Add some styles in the plugin’s `styles` directory (chose one from the hundreds in the [official repository](https://github.com/citation-style-language/styles/)), e.g. for APA:
   ```bash
   wget https://raw.githubusercontent.com/citation-style-language/styles/master/apa.csl -P ./plugins/eleventy-plugin-scholar/styles/
   ```
4. Add locales (chose one from the [official repository](https://github.com/citation-style-language/locales)), e.g. en-US :
   ```bash
   wget https://github.com/citation-style-language/locales/raw/master/locales-en-US.xml -P ./plugins/eleventy-plugin-scholar/locales/
   ```
5. Use in your own eleventy templates, thanks to the `fullBibliography` function (make sure to mark as `safe` for HTML to be rendered, not escaped):
   ```njk
   {{ "/path/to/bib.json" | fullBibliography | safe }}
   ```

More on the way soon!

## License

MIT