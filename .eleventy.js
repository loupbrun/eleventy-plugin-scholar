const fullBibliography = require("./src/full-bibliography");  

module.exports = function(eleventyConfig) {  
    eleventyConfig.addFilter("fullBibliography", fullBibliography);  
};