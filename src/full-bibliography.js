const fs = require('fs');
const path = require('path');
const CSL = require('citeproc');

module.exports = function(file) {
    let relativeFilePath = `.${file}`
    
    if (!file || !file.length) {
        console.info('No bibliography provided, silently going back to sleep.')
        return
    }

    if (path.extname(file) != '.json') {  
        throw new Error("eleventy-scholar requires a filetype of JSON. Please export a valid CSL-JSON.");  
    }
  
    // fetch bibliography
    let rawBib = fs.readFileSync(relativeFilePath)
    
    let jsonBib = JSON.parse(rawBib)
    
    let items = {}
    let itemIDs = []
    for (item of jsonBib) {
        items[item.id] = item;
        itemIDs.push(item.id)
    }

    // which citation style?
    // TODO: allow user to specify citation style/file
    let styleID = 'apa'
    // IMPORTANT: you must specify the **encoding** (utf8) in order to retrieve the content
    // or else, the funciton will only return a *buffer* (kinda useless)
    let style = fs.readFileSync(path.resolve(__dirname, '../styles/apa.csl'), 'utf8')

    // which locale?
    // TODO: allow user to specify locale langauge/file
    let localeID = 'en-US'
    // IMPORTANT: you must specify the **encoding** (utf8) in order to retrieve the content
    // or else, the funciton will only return a *buffer* (kinda useless)
    let locale = fs.readFileSync(path.resolve(__dirname, '../locales/locales-en-US.xml'), 'utf8')

    // instantiate the processor!
    let citeproc = new CSL.Engine({
        retrieveLocale: function (lang) {
            return locale
        },
        retrieveItem: function (itemID) {
            return items[itemID];
        }
    }, style)
    
    // crucial part: out items in the bibliography!
    citeproc.updateItems(itemIDs)
    
    // filter the bibliography output
    // @see https://citeproc-js.readthedocs.io/en/latest/running.html#makebibliography
    let bibFilter = [
        {
            maxoffset: 0,
            entryspacing: 12,
            linespacing: 0,
            hangingindent: true,
            'second-field-align': false,
            bibstart: "<div class=\"csl-bib-body mb-3\">\n",
            bibend: "</div>",
            bibliography_errors: []
        }
    ]

    // process the cluster with our instantiated processor
    let result = citeproc.makeBibliography(bibFilter)
    
    // output here!
    return result[1].join('\n\n')
//    return 'Bibliography WIP'
};
